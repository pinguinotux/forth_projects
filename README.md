# My AmForth #

**AmForth** Se puede compilar para diferentes arquitecturas de microcontroladores,
está soportado para varios microcontroladores de la familia **AVR**, como es el 
caso del atmega328p (Arduino UNO) y el atmega1280 (Arduino Mega).

Las prestaciones que puede tener un interprete en un microcontrolador AVR son
interesantes ya que solo se requiere compilar una vez el firmware para luego
hacer uso del hardware y configuración en tiempo de ejecución sin tener que pasar
nuevamente por el compilador.

## Creando firmware para ATMEGA328P ##

Para compilar el firmware del atmega328p se puede hacer uso del makefile que se
encuentra en el directorio:

```
amforth-6.3/appl/myProyect
```

El compilador que se usa requiere de wine (Emulador de windows para Linux).
El compilador denominado avr2asm se encuentra en el siguiente directorio.

```
amforth-6.3/avr8/Atmel
```

Dentro del makefile mencionado se puede ver la línea 42 con el siguiente contenido:

```
AVRASM=wine $(DIR_ATMEL)/avrasm2.exe -I $(DIR_ATMEL)/Appnotes2 
```





# Arduino uno con AmForth #

Este repositorio contiene la información necesaria para instalar AmForth en Arduino.
Para este caso será un Arduino Uno.


### Descargas ###

Para descargar la versión de AmForth se debe dirigir al siguiente enlace:

https://sourceforge.net/projects/amforth/files/amforth/

Luego de haber descargado la version amforth-x.x.tar.gz debe desempaquetarla en el directorio deseado.

Además, debe instalar el avrdude

```
sudo apt-get install avrdude
```


### Para tener en cuenta ###

La configuración de los pines del microcontrolador ATMega328 con los pines del Arduino Uno se muestra en la siguiente figura.

![pines.png](https://bitbucket.org/repo/a85Rk4/images/1325797883-pines.png)

Para lo cual es necesario entender cómo cambiar el modo del pin (I/O) de los pines:

![estados.png](https://bitbucket.org/repo/a85Rk4/images/2434203158-estados.png) 
### Who do I talk to? ###

* Carolina Pulido
* Johnny Cubides
