.. _Technical Guide:

===============
Technical Guide
===============

.. toctree::
   :maxdepth: 2

   FirstSteps
   Hardware
   Sources
   Architecture
   Compiler
   Standards
   AVR8
   MSP430
   Tools
