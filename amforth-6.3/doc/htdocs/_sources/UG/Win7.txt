Hi list,
I recently found amforth and wanted to give it a try. I prefer the approach of an embedded interpreter
to a traditional edit-compile-download-test cycle.
First, I had the usual troubles with the fuses (I tried to program the device from within Atmel Studio, which always gave
me verification errors.)
Then I copied&pasted the commands (assembler and avrdude) from the Linux Makefile in appl/arduino
to a DOS box and that worked fine.
The next step was to modify the Makefile so that it works on Windows/cygwin.
Here it is, I hope it is useful.
My environment is: Windows7 and Atmel Studio 7. Tested with an duemilanove with m168 and a UNO clone.
The Makefile needs only small modifications:
* use a relative path for AMFORTH, avrasm2 does not understand absolute
  cygwin paths
* do not use an explicit port for avrdude
* I am using Atmel Studio 7, the device files are in
  AtmelStudio7.0packsatmelATmega_DFP1.0.90avrasminc
* I have a couple of duemilanove's, and the all have m168 MCUs, so i introduced the target duemilanove168.
  The sources are exactly the same, only the definitions in the Makefile differ.
* with the UNO, I had to remove the definition of SPMEN in device.asm, because Atmel defines it also.
Regards,
Dieter
-- cut here --
# Simple makefile for building the 
# Arduino amforth vor various targets
# Examples of usage for Arduino leonardo:
#
# 1) Assemble the whole flash and eemprom files
#     make leonardo.hex
#
# 2) Backup the current flash & eeprom values 
#     make leonardo.bak
#
# 3) Erase the whole MCU Flash
#    make leonardo.era
#
# 4) Upload the new firmware using the hex file generated
#    make leonardo
#
# 5) Set the appropiate MCU fuses
#    make leonardo.fuse
#
# 6) Clear files (except backup)
#    make leonardo.clr
SHELL=/bin/bash
##############################
# TARGET DEPENDANT VARIABLES #
##############################
# 1) MCU should be identical to the device
#    Look at the .../avr8/devices/ folder
# 2) PART is the device model passed to avrdude.
# 3) LFUSE, HFUSE, EFUSE are the device-specific fuses
#    there is a useful fuse calc tool at:
#    http://www.engbedded.com/fusecalc/
# --------------------------------------
# Example fuse settings for 'leonardo'
# Low Fuse LFUSE=0xFF
#  - No Div8 prescaler, 
#  - No ouptput Clock, 
#  - Low Crystal mode: >=8 MHz + start-up time: 16K CK cycles + 65 ms
# High Fuse HFUSE=0xD9
# - Enable Serial Programming & Downloading
# - Bootsize 2048 words (4096 bytes)
# Extended Fuse EFUSE=0xF9
# - Brown-out detection @ 3.5V
# - no Hardware Boot Vector (=boot at $0000)
# --------------------------------------
leonardo:         PART=m32u4
leonardo.hex:     MCU=atmega32u4
leonardo.era:     PART=m32u4
leonardo.bak:     PART=m32u4
leonardo.fuse:    PART=m32u4
leonardo.fuse:    LFUSE=0xFF
leonardo.fuse:    HFUSE=0xD9
leonardo.fuse:    EFUSE=0xE9
uno:              PART=m328p
uno.hex:          MCU=atmega328p
uno.era:          PART=m328p
uno.bak:          PART=m328p
uno.fuse:         PART=m328p
uno.fuse:         LFUSE=0xFF
uno.fuse:         HFUSE=0xD9
uno.fuse:         EFUSE=0xFD
mega128:        PART=m1280
mega128.hex:    MCU=atmega1280
mega128.era:    PART=m1280
mega128.bak:    PART=m1280
mega128.fuse:   PART=m1280
mega128.fuse:   LFUSE=0xFF
mega128.fuse:   HFUSE=0xD9
mega128.fuse:   EFUSE=0xF7
sanguino:         PART=m644p
sanguino.hex:     MCU=atmega644p
sanguino.era:     PART=m644p
sanguino.bak:     PART=m644p
sanguino.fuse:    PART=m644p
sanguino.fuse:    LFUSE=0xFF
sanguino.fuse:    HFUSE=0xF9
sanguino.fuse:    EFUSE=0xFD 
duemilanove:      PART=m328p
duemilanove.hex:  MCU=atmega328p
duemilanove.era:  PART=m328p
duemilanove.bak:  PART=m328p
duemilanove.fuse: PART=m328p
duemilanove.fuse: LFUSE=0xFF
duemilanove.fuse: HFUSE=0xD9
duemilanove.fuse: EFUSE=0x05
duemilanove168:      PART=m168
duemilanove168.hex:  MCU=atmega168
duemilanove168.era:  PART=m168
duemilanove168.bak:  PART=m168
duemilanove168.fuse: PART=m168
duemilanove168.fuse: LFUSE=0xFF
duemilanove168.fuse: HFUSE=0xDD
duemilanove168.fuse: EFUSE=0xF9
diecimila:        PART=m168
diecimila.hex:    MCU=atmega168
diecimila.era:    PART=m168
diecimila.bak:    PART=m168
diecimila.fuse:   PART=m168
diecimila.fuse:   LFUSE=0xFF
diecimila.fuse:   HFUSE=0xDD
diecimila.fuse:   EFUSE=0xF9
# AMFORTH VERSION TO USE
# 'code' for trunk and x.y for the releases (i.e 5.0)
#VERSION=5.0
VERSION=code
# Under Windows and cygwin and with the ATMEL assembler avrasm2.exe,
# AMFORTH should be a relative path, e.g. "../.."
# avrasm2 dow not understand absolute cygwin paths with "/cygdrive"
CORE=$(AMFORTH)/avr8
# directories
ifneq ($(OS),Windows_NT)
    ATMEL=$(AMFORTH)/avr8/Atmel
else
    ATMEL=/cygdrive/C/Atmel/Studio/7.0/toolchain/avr8/avrassembler
endif
# ------------------------
# PROGRAMMER CONFIGURATION
# ------------------------
PROGRAMMER=avrisp2
PORT=/dev/ttyUSB0
AVRDUDE=avrdude
ifneq ($(OS),Windows_NT)
    AVRDUDE_FLAGS=-q -P $(PORT) -c $(PROGRAMMER)
else
    AVRDUDE_FLAGS=-q -c $(PROGRAMMER)
endif
# ----------------
# ASSEMBLER TO USE
# ----------------
ifneq ($(OS),Windows_NT)
    AS_INCLUDE=-I $(ATMEL)/Appnotes2 -I$(CORE)  -I$(CORE)/devices/ -I$(AMFORTH)/common
else
    ATMEL_DEVICES='c:AtmelStudio7.0packsatmelATmega_DFP1.0.90avrasminc'
    AS_INCLUDE=-I $(ATMEL_DEVICES) -I$(CORE)  -I$(CORE)/devices/ -I$(AMFORTH)/common
endif
ifneq ($(OS),Windows_NT)
    ASM=wine $(ATMEL)/avrasm2.exe
else
    ASM=$(ATMEL)/avrasm2.exe
endif
# flags Specific to avrasm2.exe
AS_FLAGS=$(AS_INCLUDE) -fI -v0
#ASM=avra $(AS_FLAGS)
#--------------------------
# Generic assemble patterns
#--------------------------
# Assemble the target
%.hex : %.asm
 @echo "Producing Hexfiles for Arduino $*" 
 @$(ASM) $(AS_FLAGS) -I $(CORE)/devices/$(MCU) -e $*.eep.hex -m $*.map -l $*.lst $
------------------------------------------------------------------------------
