# Digital Ports #

ATMegas tienen puertos digitales cada uno con 8 pines individuales.
Pueden ser configurados como pines de entrada y salida. Para hacer
un uso fácil de ellos, amforth tiene una pequeña biblioteca
bitnames.frt en el directorio lib.

El puerto de nombre indica que sólo se pueden usar puertos IO con esta
biblioteca. Dado que las direcciones utilizadas son direcciones 
RAM, se puede utilizar toda la dirección, no sólo el rango IO. Se 
accede a las direcciones en el nivel de byte. Para bits simples 
(portpin: definiciones) el número de bit puede exceder los 8 bits 
que un byte puede retener. En este caso, la dirección se incrementa 
hasta un valor que contiene el bit especificado: p. El bit 24 de la 
dirección 80 es el mismo que el bit 0 de la dirección 83. Los mapas 
de bits están enlazados al byte al que se dirigen.

El DDR se encarga de definir el modo del pin donde 1 es salida y 0
en entrada. Por ejemplo, si se quiere utilizar como salida los pines 
8, 9 y 12 de los pines digitales del arduino UNO se tiene que:

Arduino						13		12		11		10		9		8
Atmega328	10		9		19		18		17		16		15		14
DDRB		DDRB7	DDRB6	DDRB5	DDRB4	DDRB3	DDRB2	DDRB1	DDRB0
PORTB		PORTB7	PORTB6	PORTB5	PORTB4	PORTB3	PORTB2	PORTB1	PORTB0

Opciones a considerar:
----------------------

Según el valor del bit para el puerto B.

DDRB		0		0		0		1		0		0		1		1
PORTB		0		0		0		0		0		0		0		0
PORTB		0		0		0		0		0		0		0		1
PORTB		0		0		0		0		0		0		1		0
PORTB		0		0		0		0		0		0		1		1
PORTB		0		0		0		1		0		0		0		0
PORTB		0		0		0		1		0		0		0		1
PORTB		0		0		0		1		0		0		1		0
PORTB		0		0		0		1		0		0		1		1


En hexadecimal:

DDRB		1		3
PORTB		0		0
PORTB		0		1
PORTB		0		2
PORTB		0		3
PORTB		1		0
PORTB		1		1
PORTB		1		2
PORTB		1		3

De tal manera que para programar en amforth se tiene que para el DDRB
que corresponde a la dirección 0x24 se debe escribir 13 en
hexadecimal así:

```
> $24 constant DDRB
  ok
> $13 DDRB c!
  ok
>
```

Ahora si se conecta un led en los pines 8-13 del arduino sólo
podremos prender los leds de los pines 8, 9 y/o 12 pues son los
únicos que están configurados como salida. En estos pines se escribe
un 1 lógico o un cero lógico según se desee, si en este caso el
circuito responde a lógica positiva se encenderán el led 8 y 12
cuando se escribe 11 en el PORTB que corresponde a la dirección 0x25:

```
> $25 constant PORTB
  ok
> $11 PORTB c!
  ok
>
```

Las direcciones en memoria para los puertos son:
```
> $25 constant PORTB
  ok
> $24 constant DDRB
  ok
> $23 constant PINB
  ok
> $28 constant PORTC
  ok
> $27 constant DDRC
  ok
> $26 constant PINC
  ok
> $2B constant PORTD
  ok
> $2A constant DDRD
  ok
> $29 constant PIND
  ok
>
```

De igual manera que en los casos anteriores el PINxn [7:0] representa 
en hexadecimal a los pines a los que se les hace un toggle de estado
con un 1 y 0 para no cambiarles el estado.


