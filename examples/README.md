# Desde la terminal#

Para programar desde la terminal abrimos minicom para comunicación
serial con el microcontrolador donde el prompt de amforth en >

```
$ minicom -b 38400 -D /dev/ttyACM0  
```


## Ejemplos ##


### 1. Hello World ###
En caso de querer introducir un texto dentro de la definición de la
palabra se utiliza ." mi texto " como se muestra en el siguiente
hola mundo. (El comando cr indica un salto de línea)

```
> : hello ." Hello World! Do you speak Forth? " cr ;
 ok
> hello
Hello World!
 ok
```

### 2. Blink un LED de prueba###

Para encender y apagar el led del pin digital 13 en el arduino uno
se utilizan las siguientes tres líneas.

```
> $20 $24 c!
> $20 $25 c!
> $00 $25 c!
```

Sin embargo, para entender mejor el código se crean etiquetas para los
números, es decir, se crean constantes según lo que representan.

```
> $25 constant PORTB
> $24 constant DDRB
```

El arduino utiliza su propio esquema de numeración para los pines,
pero por ahora utilizamos el atmega uno: digital-13 es el mismo que
el bit 7 del puerto B. El puerto B tiene 8 pines y tres registros
para configurarlos. Necesitamos dos de ellos: El Registro de
Dirección de Datos (DDR) y el Registro de PUERTO (Salida). El tercer
registro se utiliza para leer desde el puerto (PIN).

Los comandos anteriores se pueden escribir ahora como

```
> $20 DDRB c!
> $20 PORTB c!
> $00 PORTB c!
```

Para facilitar aún más los comando se puede configurar el Data
Directionner Register DDR así:

En Arduino:
```
void setup(){
	pinMode(13, OUTPUT);	
}
```

En Forth:
```
> : led-init $20 DDRB c! ;
 ok
>
```

El anterior comando establece el séptimo bit de PORTB como pin de
salida.

Ahora para encender el LED (en Forth):
```
> : led-on $20 PORTB c! ;
 ok
>
```

Ahora para apagar el LED (en Forth):
```
> : led-off $00 PORTB c! ;
 ok
>
```

Utilizando las palabras creadas (led-init, led-on, led-off) se puede 
prender y apagar el led de forma manual:

```
> led-init
 ok
> led-on
 ok
> led-off
 ok
>
```

Finalmente, si queremos hacer un blink en donde podamos ver
parpadear manteniendo un estado o el otro durante cierto tiempo
automáticamente tendremos algo como esto:

```
> : led-blink led-on 500 delay-ms led-off 500 delay-ms ;
 ok
>
```

Para lo cual tenemos que definir la palabra delay-ms así:
```
> : delay-ms ( n -- )
 ok 0 ( n 0 )
 ok do
 ok  1ms
 ok loop ;
 ok
>
```

La forma como se definió delay-ms es básicamente un for donde recibe
como parámetro el valor de n que es el número máximo hasta el cual
va a contar, e.a. 500 delay-ms -> genera un delay de 500 ms. (ver
counter)


Y si se quiere un blink forever que no devuelve el prompt hasta que
no se presione una tecla se tiene que:

```
> : blink-forever 
 ok ." press any key to stop "
 ok begin
 ok  led-blink
 ok  key?
 ok until
 ok key drop
 ok ;
 ok
>
```



### 3. Operaciones con enteros ###

Las siguientes operaciones aritméticas tienen la estructura
"postfix" que indica que la palabra se escribe después de sus
parámetros, caso contrario es "infix" donde la palabra se encuentra 
en medio de los número como se escribe en lenguaje matemático. 
Para estos casos recibe dos parámetros que son números
antes de la palabra (+ o - o * o / o /MOD o MOD). Los enteros no pueden ser 
mayores que +2147483647 o menores que -2147483648.

#### 3.1 Sumar ####
+            ( n1 n2 -- sum )

```
> 2 3 + .
5 ok
> 3 5 + 8 + .
16 ok
>
```

#### 3.2 Restar ####
-           ( n1 n2 -- diff )

```
> 2 3 - .
1 ok
> 8 3 - 8 - .
-3 ok
>
```

#### 3.3 Multiplicar ####
*           ( n1 n2 -- mul )

```
> 2 3 * .
6 ok
> 2 4 * 2 * .
16 ok
>
```

#### 3.4 Dividir ####
/            ( n1 n2 -- div )

```
> 2 3 / .
0 ok
> 12 2 / 3 / .
2 ok
>
```

Por ejemplo si quisiera desarrollar la siguiente operación: 

*( ((3x2) + (4x8) +5)/2 ) + 3*
```
> 3 2 * 4 8 * + 5 + 2 / 3 + .
24 ok
>
```

#### 3.5 Módulo ####
/MOD         ( n1 n2 -- rem )

```
> 8 2 /MOD .
4  ok
> 8 2 /MOD 2 /MOD .
2  ok
>
```

#### 3.5 Residuo del módulo ####
MOD         ( n1 n2 -- rem )

```
> 12 7 MOD .
5  ok
> 12 7 MOD 3 MOD .
2  ok
>
```

#### Counter ####

```
> : count ( n -- )
 ok 0 (n 0 )
 ok do
 ok  i . cr
 ok loop ;
 ok 
> 5 count
0
1
2
3
4
 ok
> 3 count
0
1
2
 ok
>
```


## Ayuda extra  ##

Cuando se crean palabras se pueden utilizar hasta 31 caracteres para
definir la palabra dentro del diccionario. Sin embargo, no se pueden
utilizar las siguientes palabras:
* return -> final de entrada
* backspace -> corregir un error
* space -> introduce un espacio

### Otras palabras ###
* number . -> imprime el number
* number spaces -> agrega number espacios
* number emit -> pinta el caracter representado por number (e.a. 42
  emit)
* 



## Registros ##

